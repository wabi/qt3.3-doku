# Qt 3.3 Dokumentation
This repository is a copy of the official Qt 3.3 documentation. We hold this copy here as the original online version disappears from time to time.

As the time of writing this, it is available under: [https://doc.qt.io/archives/3.3/classes.html](https://doc.qt.io/archives/3.3/classes.html)

Just clone this repo locally, head over to the `public/` folder and open the `index.html` in a browser of your choice.

Note: Copy hosted on gitlab pages: [https://wabi.gitlab.io/qt3.3-doku](https://wabi.gitlab.io/qt3.3-doku)